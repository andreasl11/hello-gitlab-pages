---
title: README
---
# Hello Gitlab Pages
My first serious attempt to create a gitlab pages site.

Yeah. That all together did it. I had to specify the properties 'baseurl' and 'url' in the
file `_config.yaml`. I find that setting up both gitlab ci and jekyll is semi-involved and you have
to get the configuration right, or else ci jobs will fail, the site will not be created or the css
theme is broken. For instance, I had a hard time figuring out that markdown pages you want to render
have to have a `yaml` front matter, or that you have to set `baseurl` and `url` in `jekyll`'s
`_config.yaml` in order for the `css` to work. wtf.

I am...

    apt install fancypants
    brew install fancytoo

   - happy that it works
   - unhappy how it looks
   - unhappy what it entails
     - including markdonw specifics

Cheers!
