---
title: Yeah! The Title
---
# Yeah!

This page lives in the folder public.
Useful resources:

    - https://docs.gitlab.com/ee/user/project/pages/
    - https://docs.gitlab.com/ee/user/project/pages/getting_started/new_or_existing_website.html
    - https://docs.gitlab.com/ee/user/project/pages/getting_started_part_four.html
    - https://gitlab.com/jekyll-themes/default-bundler/tree/master
    - https://www.youtube.com/playlist?list=PLLAZ4kZ9dFpOPV5C5Ay0pHaa0RJFhcmcB

...Or, without indentation:
- https://docs.gitlab.com/ee/user/project/pages/
- https://docs.gitlab.com/ee/user/project/pages/getting_started/new_or_existing_website.html
- https://docs.gitlab.com/ee/user/project/pages/getting_started_part_four.html
- https://gitlab.com/jekyll-themes/default-bundler/tree/master
- https://www.youtube.com/playlist?list=PLLAZ4kZ9dFpOPV5C5Ay0pHaa0RJFhcmcB

I could go on
and try something else

I mean `something weird`.

See, if this works:
```bash
apt install -y foo
rm -fr bar
```

And *stuff*. Or **stuff**. Or, ***stuffffff***. YOu get it

1. one
2. two
3. three

let's see...
